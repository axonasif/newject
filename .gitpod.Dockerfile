FROM gitpod/workspace-full:latest

RUN bash -c "NV='14.8.0' && source .nvm/nvm.sh && nvm install $NV && nvm use $NV && nvm alias default $NV"

RUN echo "nvm use default &>/dev/null" >> ~/.bashrc.d/51-nvm-fix
